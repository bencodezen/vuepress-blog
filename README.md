# VuePress Blog Boilerplate

This is an ever-changing and opinionated architecture that uses [VuePress](https://www.vuepress.vuejs.org) to power your blogging platform.

> The repo has been moved over to GitHub: https://github.com/bencodezen/vuepress-blog-boilerplate/
